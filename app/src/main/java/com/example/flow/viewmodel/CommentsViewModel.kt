package com.example.flow.viewmodel


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.flow.model.CommentModel
import com.example.flow.network.AppConfig
import com.example.flow.network.CommentApiState
import com.example.flow.network.Status
import com.example.flow.repository.CommentsRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch

class CommentsViewModel : ViewModel() {

    private val repository = CommentsRepository(
        AppConfig.ApiService()
    )

    val commentState = MutableStateFlow(
        CommentApiState(
            Status.LOADING,
            CommentModel(), ""
        )
    )

    init {

        getNewComment(1)
    }


    fun getNewComment(id: Int) {

        commentState.value = CommentApiState.loading()

        viewModelScope.launch {

            repository.getComment(id)

                .catch {
                    commentState.value =
                        CommentApiState.error(it.message.toString())
                }

                .collect {
                    commentState.value = CommentApiState.success(it.data)
                }
        }
    }
}