package com.example.flow.repository

import com.example.flow.model.CommentModel
import com.example.flow.network.ApiService
import com.example.flow.network.CommentApiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn


class CommentsRepository(private val apiService: ApiService) {
    suspend fun getComment(id: Int): Flow<CommentApiState<CommentModel>> {
        return flow {
            val comment=apiService.getComments(id)
            emit(CommentApiState.success(comment))
        }.flowOn(Dispatchers.IO)
    }
}